pyexcel-ods (0.6.0-0kali1) kali-dev; urgency=medium

  [ Kali Janitor ]
  * Trim trailing whitespace.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update standards version to 4.5.0, no changes needed.

  [ Ben Wilson ]
  * Update email address
  * Add Uploaders

  [ Arnaud Rebillout ]
  * New upstream version 0.6.0
  * Refresh patches
  * Rework patch that remove test files
  * Use description_file instead of description-file
  * Update copyright years
  * Clarify which Build-Dependencies are needed for tests only
  * Tighten dependency on pyexcel-io
  * Bump debhelper-compat
  * Use execute_after in debian/rules

 -- Arnaud Rebillout <arnaudr@kali.org>  Fri, 23 Aug 2024 14:17:21 +0700

pyexcel-ods (0.5.6-0kali2) kali-dev; urgency=medium

  [ Sophie Brun ]
  * Remove obsolete debian/README.Debian
  * Remove Python 2 module
  * Bump Standards-Version to 4.4.1

  [ Raphaël Hertzog ]
  * Add GitLab's CI configuration file
  * Configure git-buildpackage for Kali
  * Update URL in GitLab's CI configuration file

 -- Sophie Brun <sophie@offensive-security.com>  Fri, 24 Jan 2020 08:40:30 +0100

pyexcel-ods (0.5.6-0kali1) kali-dev; urgency=medium

  [ Raphaël Hertzog ]
  * Update Maintainer field
  * Update Vcs-* fields for the move to gitlab.com

  [ Sophie Brun ]
  * Add debian/gbp.conf
  * New upstream version 0.5.6
  * Use debhelper-compat 12
  * Bump Standards-Verison to 4.3.0
  * Update debian/copyright
  * Build python3 package
  * Update documentation files
  * Add a patch to remove files created during tests

 -- Sophie Brun <sophie@offensive-security.com>  Thu, 11 Jul 2019 09:14:16 +0200

pyexcel-ods (0.1.1-0kali2) kali-dev; urgency=medium

  * Add missing doc README.rst

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 29 Aug 2017 12:14:30 +0200

pyexcel-ods (0.1.1-0kali1) kali-dev; urgency=medium

  * Initial release

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 29 Aug 2017 11:05:00 +0200
